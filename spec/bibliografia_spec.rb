require 'spec_helper'
require 'lib/bibliografia.rb'

describe Bibliografia do
    before :each do 
        @book = Libro.new({:autor => 'Autor1', :titulo => 'Titulo', :serie => 'Serie', :editorial => 'Editorial', :edicion => 'Edicion', :f_pub => 'Fecha', :isbn => 'ISBN'})
        @book1 = Libro.new({:autor => ["Autor1", "Autor2", "Autor3"], :titulo => "Titulo", :serie => "Serie", :editorial => "Editorial", :edicion => "Edicion", :f_pub => "Fecha", :isbn => ["ISBN", "ISBN1", "ISBN2"]})
    end  
    
    describe "Mostrar datos del libro" do
        it "Debe existir uno o mas autores." do
            expect(@book.autor).to eq('Autor1')
        end
                
        it "Debe existir un titulo" do
            expect(@book.titulo).to eq('Titulo')
        end
                
        it "Debe existir o no una serie" do
            expect(@book.serie).to eq('Serie')
        end
                
        it "Debe existir una editorial" do
            expect(@book.editorial).to eq('Editorial')
        end
                
        it "Debe existir un numero de edicion" do
            expect(@book.edicion).to eq('Edicion')
        end
                
        it "Debe existir una fecha de publicacion" do
            expect(@book.f_pub).to eq('Fecha')
        end
                
        it "Debe existir uno o mas numeros ISBN" do
            expect(@book.isbn).to eq('ISBN')
        end
        
       
        it "Existe un metodo para obtener el listado de autores" do
            expect(@book1.autor).to eq(['Autor1', 'Autor2', 'Autor3'])
        end
                
        it "Existe un metodo para obtener el tıtulo" do
            expect(@book1.titulo).to eq('Titulo')
        end
                
        it "Existe un metodo para obtener la serie" do
            expect(@book1.serie).to eq('Serie')
        end
                
        it "Existe un metodo para obtener la editorial" do
            expect(@book1.editorial).to eq('Editorial')
        end
                
        it "Existe un metodo para obtener el numero de edicion" do
            expect(@book1.edicion).to eq('Edicion')
        end
                
        it "Existe un metodo para obtener la fecha de publicacion" do
            expect(@book1.f_pub).to eq('Fecha')
        end
                
        it "Existe un metodo para obtener el listado de ISBN" do
            expect(@book1.isbn).to eq(['ISBN', 'ISBN1', 'ISBN2'])
        end
                
        it "Existe un metodo para obtener la referencia formateada" do
            expect(@book1.to_s).to be == "Autor1, Autor2, Autor3.\nTitulo\nSerie\nEditorial;\nEdicion\n(Fecha)\nISBN\nISBN1\nISBN2\n"
        end
    end    
end

